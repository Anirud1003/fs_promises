const fs = require("fs")
const path = require("path")

const random = Math.floor(Math.random() * (10 - 1) + 1)

function makeDir(path) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path, (err) => {
            console.log("Directory creation started")
            if (err) {
                reject(err)
                console.log("error occured while creating")
            } else {
                resolve()
                console.log(`directory successfully created at ${path}`)
            }
        })
    })
}

function mkFiles() {
    return new Promise((resolve, reject) => {
        const array = new Array(random).fill(0)
        let index = 1
        array.map((element) => {
            element += index++;
            const fileName = path.join(__dirname, `/random/file_${element}.json`)
            fs.writeFile(fileName,JSON.stringify("Hello"),(error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(console.log("File Created Succesfully"))
                }
            })
            return element
        })
    })
}

function rmFile() {
    return new Promise((resolve, reject) => {
        const array = new Array(random).fill(0)
        let index = 1
        array.map((element) => {
            element += index++;
            const fileName = path.join(__dirname, `/random/file_${element}.json`)
            fs.unlink(fileName, (error) => {
                if (error) {
                    reject(error)
                } else {
                    resolve()
                    console.log("file deleted")
                }
            })
        })
    })
}

const problem1 = (path) => {
    makeDir(path)
    .then(() => {
        return mkFiles()
    })
    .then(() => {
        return rmFile()
    })
    .catch(error => {
        console.error(error)
      })

}

module.exports = problem1
