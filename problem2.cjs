const fs = require("fs")
const path = require("path")
const lipsumFilePath = path.join(__dirname, "/lipsum.txt")

const readFromFile = (filepath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, "utf8", (error, data) => {
      if (error) {
        reject(error)
      } else {
        resolve(data)
      }
    })
  })
}

const deleteAllFiles = (filepath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, "utf8", (error, data) => {
      if (error) {
        reject(error)
      } else {
        files = data.split("\n")
        files.slice(0, files.length - 1).map((file) => {
          fs.unlink(file, (error) => {
            if (error) {
                reject(error)
            } else {
                resolve()

            }
          }) 
        })
      }
    })
  })
}

const writeDataToFile = (filepath, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(filepath, data, "utf8", (error) => {
      if (error) {
        reject(error)
      } else {
        resolve()
      }
    })
  })
}

const addNames = (filepath, data) => {
  return new Promise((resolve, reject) => {
    fs.appendFile(filepath, data, "utf8", (error) => {
      if (error) {
        reject(error)
      } else {
        resolve()
      }
    })
  })
}

function problem2() {
  readFromFile(lipsumFilePath)
    .then((lipsumData) => {
        return writeDataToFile(path.join(__dirname, "upperCaseFile.txt"),lipsumData.toUpperCase())
    })
    .then(() => {
        return addNames(path.join(__dirname, "fileNames.txt"), "upperCaseFile.txt\n")
    })
    .then(() => {
        return readFromFile(path.join(__dirname, "upperCaseFile.txt"))
    })
    .then((upperCaseData) => {
        return writeDataToFile(path.join(__dirname, "lowerCaseFile.txt"),upperCaseData.toLowerCase().split(". ").join("\n"))
    })
    .then(() => {
        return addNames(path.join(__dirname, "fileNames.txt"), "lowerCaseFile.txt\n")
    })
    .then(() => {
        return readFromFile(path.join(__dirname, "lowerCaseFile.txt"))
    })
    .then((sortedData) => {
        return writeDataToFile(path.join(__dirname, "sortedFile.txt"),sortedData.split("\n").sort().join("\n"))
    })
    .then(() => {
        return addNames(path.join(__dirname, "fileNames.txt"), "sortedFile.txt\n")
    })
    .then(() => {
        return deleteAllFiles(path.join(__dirname, "fileNames.txt"))
    })
    .catch(error => {
      console.error(error)
    })
}

module.exports = problem2
